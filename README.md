# Docker Image for Meteor+Fig development

Simple image for developing app using **Meteor+Fig**, with a volume shared between the container and the host system.

## Usage
1 - Install Fig: [http://www.fig.sh/install.html](http://www.fig.sh/install.html);  
1 - Create a folder named **webapp** *(meteor create webapp)*;  
2 - Create/Copy the [fig.yml](https://bitbucket.org/jadsonlourenco/docker-meteor-dev/raw/7072f84f7ec3e42aad6aa419a1b6bce587df0d47/fig.yml) file in current folder *(out of webaap)*;  
3 - Run: **fig up**;

## Advantages
- [Passenger Image (0.9.14)](https://github.com/phusion/passenger-docker)  
- [Fig.sh](http://www.fig.sh/)  
- **Docker Volume:** no need to add the code of the app, simply set the location of volume on [fig.yml](https://bitbucket.org/jadsonlourenco/docker-meteor-dev/raw/7072f84f7ec3e42aad6aa419a1b6bce587df0d47/fig.yml) file and everything will work quickly.

## License
Apache License, Version 2.0

## By
[Jadson Lourenco](https://jadsonlourenco.com/)  
*"Quem tem verdadeiros ideais não sonha."*